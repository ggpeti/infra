# Master node

resource "digitalocean_droplet" "rdspg_master" {
  name               = "rdspg-master"
  size               = "s-1vcpu-2gb"
  image              = "ubuntu-16-04-x64"
  region             = var.do_region
  ssh_keys           = [digitalocean_ssh_key.rdspg.id]
  ipv6               = true
  private_networking = true

  connection {
    user        = "root"
    host        = digitalocean_droplet.rdspg_master.ipv4_address
    private_key = tls_private_key.rdspg.private_key_pem
  }

  provisioner "file" {
    source      = "templates/import_host.nix"
    destination = "/root/import_host.nix"
  }

  provisioner "remote-exec" {
    inline = [
      "echo -e 'silent\nshow-error\nretry=2' | tee ~/.curlrc > /dev/null",
      "wget 'https://raw.githubusercontent.com/elitak/nixos-infect/de3ef4794110f811f278643aa46ab3cce41a30c1/nixos-infect' -O /root/nixos-infect",
      "echo 'Installing NixOS. Logging to /tmp/infect.log.'",
      "hostname=rdspg0 NIXOS_IMPORT='/root/import_host.nix' NIX_CHANNEL=nixos-19.03 bash /root/nixos-infect 2>&1 > /tmp/infect.log",
    ]
    on_failure = "continue"
  }
}


resource "digitalocean_floating_ip" "rdspg_master" {
  droplet_id = digitalocean_droplet.rdspg_master.id
  region     = digitalocean_droplet.rdspg_master.region
}


resource "null_resource" "rdspg_deploy_master" {
  triggers = {
    droplet = digitalocean_droplet.rdspg_master.id
    always  = uuid()
  }

  connection {
    user        = "root"
    host        = digitalocean_floating_ip.rdspg_master.ip_address
    private_key = tls_private_key.rdspg.private_key_pem
  }

  provisioner "file" {
    source      = "templates/import_host.nix"
    destination = "/root/import_host.nix"
  }

  provisioner "file" {
    source      = "templates/migrate.sql"
    destination = "/root/migrate.sql"
  }

  provisioner "file" {
    content = templatefile("templates/master.nix", {
      slave_count = var.slave_count
      slave_addrs = digitalocean_droplet.rdspg_slave.*.ipv4_address_private
      self_addr   = digitalocean_droplet.rdspg_master.ipv4_address_private
    })
    destination = "/root/host.nix"
  }

  provisioner "remote-exec" {
    inline = ["nixos-rebuild switch --show-trace"]
  }
}
