{ lib, ... }:
{
  imports = lib.optional (builtins.pathExists /root/host.nix) /root/host.nix;
}
