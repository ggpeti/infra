# NixOS module for rdspg cluster
# Provisioned by Terraform

{ config, pkgs, lib, ... } :
{
  networking.firewall.allowedTCPPorts = [ 22 5432 8080 ];
  networking.interfaces.eth1.ipv4.addresses = [ { address = "${self_addr}"; prefixLength = 16; } ];
  programs.bash.enableCompletion = true;

  services.postgresql = rec {
    enable = true;
    enableTCPIP = true;
    package = pkgs.postgresql_11;
    dataDir = "/data/postgresql";
    extraPlugins = [ package.pkgs.pg_partman ];
    extraConfig = ''
      # Partitioning
      shared_preload_libraries = 'pg_partman_bgw'
      pg_partman_bgw.dbname = 'rdspg'
      pg_partman_bgw.role = 'partman'
      pg_partman_bgw.interval = 10

      # Replication
      wal_level = replica
      wal_log_hints = on
      max_wal_senders = ${slave_count * 2}
      wal_keep_segments = 32
    '';
    authentication = lib.mkBefore ''
      %{ for addr in slave_addrs ~}
      host replication replicator ${addr}/32 trust
      %{ endfor ~}
    '';
  };

  systemd.services.rdspg-migrate = {
    serviceConfig.Type = "oneshot";
    wantedBy = [ "multi-user.target" ];
    after = [ "postgresql.service" ];
    serviceConfig.User = "postgres";
    script = ''
      $${config.services.postgresql.package}/bin/createuser rdspg || true
      $${config.services.postgresql.package}/bin/createdb rdspg -O rdspg || true
      $${config.services.postgresql.package}/bin/createuser -l partman || true
      $${config.services.postgresql.package}/bin/createuser -l --replication replicator || true
      $${config.services.postgresql.package}/bin/psql -f $${/root/migrate.sql}
    '';
  };

  services.pgmanage = {
    enable = true;
    connections.rdspg = "hostaddr=127.0.0.1 port=5432 dbname=rdspg";
    superOnly = false;
    localOnly = false;
  };
}
