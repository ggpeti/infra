# NixOS module for rdspg cluster
# Provisioned by Terraform

{ config, pkgs, lib, ... } :
{
  networking.firewall.allowedTCPPorts = [ 22 5432 8080 ];
  networking.interfaces.eth1.ipv4.addresses = [ { address = "${self_addr}"; prefixLength = 16; } ];
  programs.bash.enableCompletion = true;

  systemd.services.rdspg-replicate = {
    wantedBy = [ "multi-user.target" ];
    before = [ "postgresql.service" ];
    serviceConfig = {
      Type = "oneshot";
      User = "postgres";
      PermissionsStartOnly = true;
    };
    preStart = ''
      mkdir -m 0700 -p $${config.services.postgresql.dataDir}
      chown -R postgres:postgres $${config.services.postgresql.dataDir}
    '';
    script = ''
      if [ -z "$(ls -A $${config.services.postgresql.dataDir})" ]; then
      until $${config.services.postgresql.package}/bin/pg_basebackup \
        --host=${master_addr} \
        --pgdata=$${config.services.postgresql.dataDir} \
        --wal-method=stream \
        --checkpoint=fast \
        --username=replicator \
        --no-password \
        --write-recovery-conf; do echo "replication did not start yet, retrying..."; done
      fi
    '';
  };

  services.postgresql = {
    enable = true;
    enableTCPIP = true;
    package = pkgs.postgresql_11;
    dataDir = "/data/postgresql";
  };

  services.pgmanage = {
    enable = true;
    connections.rdspg = "hostaddr=127.0.0.1 port=5432 dbname=rdspg";
    superOnly = false;
    localOnly = false;
  };
}
