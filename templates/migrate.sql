-- Idempotent db migration
\c rdspg
CREATE SCHEMA IF NOT EXISTS partman;
CREATE EXTENSION IF NOT EXISTS pg_partman SCHEMA partman;
GRANT ALL ON SCHEMA partman TO partman;
GRANT ALL ON SCHEMA partman TO rdspg;
GRANT ALL ON ALL TABLES IN SCHEMA partman TO partman;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA partman TO partman;
GRANT EXECUTE ON ALL PROCEDURES IN SCHEMA partman TO partman;
GRANT ALL ON ALL TABLES IN SCHEMA partman TO rdspg;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA partman TO rdspg;
GRANT EXECUTE ON ALL PROCEDURES IN SCHEMA partman TO rdspg;
ALTER ROLE rdspg PASSWORD 'md5faa7cb2eb9922682c0917b4583b91343';
GRANT rdspg TO partman;
