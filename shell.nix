# nix-shell environment for provisioning infrastructure and deployment

{pkgs ? import ./nixpkgs-pinned.nix {}, ...} :
pkgs.stdenv.mkDerivation {
  name = "infra-env";
  buildInputs = [ (pkgs.terraform_0_12.withPlugins (p: [ p.digitalocean p.local p.null p.tls p.template ])) pkgs.jq ];
  shellHook = "terraform init";
}
