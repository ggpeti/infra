# Infrastructure definition

variable "do_token" {
  description = "Digital Ocean access token. See https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-api-v2#how-to-generate-a-personal-access-token"
}


variable "do_region" {
  default = "fra1"
}


variable "slave_count" {
  default = 2
}



provider "digitalocean" {
  token = var.do_token
}


resource "digitalocean_project" "rdspg" {
  name        = "rdspg"
  resources   = concat([digitalocean_droplet.rdspg_master.urn, digitalocean_floating_ip.rdspg_master.urn], digitalocean_droplet.rdspg_slave[*].urn)
}


resource "tls_private_key" "rdspg" {
  algorithm = "RSA"
}


resource "local_file" "private_key" {
  filename = "${path.module}/SECRET_private_key"
  content  = tls_private_key.rdspg.private_key_pem

  provisioner "local-exec" {
    command = "chmod 600 ${path.module}/SECRET_private_key"
  }
}


resource "digitalocean_ssh_key" "rdspg" {
  name       = "rdspg"
  public_key = tls_private_key.rdspg.public_key_openssh
}


output "master_ip" {
  value = digitalocean_floating_ip.rdspg_master.ip_address
}


output "slave_ips" {
  value = digitalocean_floating_ip.rdspg_slave.*.ip_address
}


output "private_key" {
  value     = tls_private_key.rdspg.private_key_pem
  sensitive = true
}
