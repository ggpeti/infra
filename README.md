Postgres cluster infrastructure on DigitalOcean

## What does this do?
It creates a PostgreSQL 11 cluster of 1 master and 2 slaves on DigitalOcean.
Features:
* [pg_partman](https://github.com/pgpartman/pg_partman) for partition management, including retention
* streaming replication over a private network between the hosts
* static public ip addresses
* idempotent migration capability via [migrate.sql](./templates/migrate.sql)
* systemd (systemctl for service management, journalctl for log viewing)
* pgmanage listening on port 8080

## How to use it?
0. Have [Nix](https://nixos.org/nix/) installed
1. Run `nix-shell`
2. Put a DigitalOcean access token in a file called `terraform.tfvars` with the following format:
    ```
    do_token = "TOKEN"
    ```
3. Run `terraform apply`

This by default creates a cluster that listens locally. To try it, log in to the master like so:
`TERM=xterm ssh -ti SECRET_private_key root@$(terraform output master_ip) sudo -u postgres psql rdspg`, and create tables and test data to your liking.

Then, check that the replication worked by logging in to one of the slaves and see it for yourself there:
`TERM=xterm ssh -ti SECRET_private_key root@$(terraform output --json slave_ips | jq -r '.[0]') sudo -u postgres psql rdspg`

If you'd like to have postgres listen on the public network, you'll need to add new authentication lines in [master.nix](./templates/master.nix) and [slave.nix](./templates/slave.nix) in the `authentication` attribute of the postgresql service. These lines will be added to pg_hba.conf on the target machines.

## How does it work?
There are 3 layers to this tool:

1. The deployer environment

    This means the tooling required to run the setup. It is managed by `nix-shell`. The definition of the deployer environment resides in [./shell.nix](./shell.nix).

2. The target infrastructure

    This means the virtual machines, static ip addresses, deployer keys etc. It is managed by `terraform apply`. The definition of the target infrastructure resides in [./common.tf](./common.tf), [./master.tf](./master.tf) and [./slave.tf](./slave.tf).

3. The target environment

    This means the software that needs to be installed and started on the target machines. It is managed by `nixos-rebuild switch` which is run by `terraform apply`. The definition of the target environment resides in the files in [./templates](./templates).

## What can this be used for?
There are many ways to use this project: as a starting point for microservice applications, high-availability clusters, auto-scaling services, versioned infrastructure, load balancing, reproducible experiments and even web apps with automatic certificate renewal.
